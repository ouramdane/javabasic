package com.formation.java.tp.test;

import static com.formation.java.tp.model.DevCategorie.BACK;
import static com.formation.java.tp.model.DevCategorie.FRONT;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.formation.java.tp.exception.EquipecompositionException;
import com.formation.java.tp.model.Developpeur;
import com.formation.java.tp.model.Employee;
import com.formation.java.tp.model.Equipe;
import com.formation.java.tp.model.Manager;
import com.formation.java.tp.service.EquipeService;

public class CreateEquipeExemple {

	public static void main(String[] args) {
		
		List<String> langages1= Arrays.asList("JAVA","ANGULAR","C#");
		List<String> langages2= Arrays.asList("JAVA","JAVASCRIPT");
		
		Developpeur developpeur1 = new Developpeur("David","Stéphane",38, false,BACK,langages1);
		
		Developpeur developpeur2 = new Developpeur("Lavigne","Benjamin",25, true,BACK,langages2);
		Developpeur developpeur3 = new Developpeur("David","Leslie",29, false,BACK,langages1);
		Developpeur developpeur4 = new Developpeur("Randon","Cedric",36, false,FRONT,langages2);
		Developpeur developpeur5 = new Developpeur("Hitachi","Valérie",22, true,FRONT,langages1);
		Developpeur developpeur6 = new Developpeur("SAI","Alex",33, true,FRONT,langages1);
		
		Manager manger1 = new Manager("MED", "Safir", 38, 11);
		Manager manger2 = new Manager("SAI", "Cedric", 34, 9);
		
        List<Employee> employees = new ArrayList<>();
        employees.add(developpeur1);
        employees.add(developpeur2);
        employees.add(developpeur3);
        employees.add(developpeur4);
        employees.add(developpeur5);
        employees.add(developpeur6);
        employees.add(manger1);
        employees.add(manger2);
        
        EquipeService equipeService = new EquipeService();
        
        Equipe equipe;
		try {
			equipe = equipeService.creerEquipe(employees);
			System.out.print(equipe);
		} catch (EquipecompositionException e) {
			e.printStackTrace();
		}
        
	}

	
	
}
