package com.formation.java.tp.test;

import com.formation.java.tp.model.DevCategorie;
import com.formation.java.tp.model.Developpeur;
import com.formation.java.tp.model.Employee;

public class OverridingExemple {

	public static void main(String[] args) {
		
		Employee developpeur = new Developpeur("David","Valerie",28, true,DevCategorie.BACK,null);
		developpeur.afficherDetails();

	}

}
