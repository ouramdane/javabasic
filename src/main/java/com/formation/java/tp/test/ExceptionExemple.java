package com.formation.java.tp.test;

import com.formation.java.tp.exception.EquipecompositionException;

public class ExceptionExemple {

	public static void main(String[] args) throws EquipecompositionException {
		// gives us just the exception and location.
		// throw new LangageException();
		// pass a message
		// throw new LangageException("Le developpeur ne maitrise pas JAVA");
		// wrap an exception
		//throw new LangageException(new RuntimeException());

		// print the stack trace on your own:
		try {
			throw new EquipecompositionException();
		} catch (EquipecompositionException e) {
			e.printStackTrace();
		}
	}
}
