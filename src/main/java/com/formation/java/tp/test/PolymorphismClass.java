package com.formation.java.tp.test;

import static com.formation.java.tp.model.DevCategorie.FRONT;
import com.formation.java.tp.model.Developpeur;
import com.formation.java.tp.model.Employee;
import com.formation.java.tp.service.Prestataire;

public class PolymorphismClass {

	public static void main(String[] args) {
		
		Developpeur dev = new Developpeur("David","Stéphane",28, true,FRONT, null);
		System.out.println (dev.isPrestataire);

		Employee employee = dev;
		employee.afficherDetails();

		Prestataire  prestataire= dev;
		System.out.println (prestataire.isPrestataire());

		//System.out.println (prestataire.isPrestataire); 
		//System.out.println (employee.isPrestataire()); 
		}


}
