package com.formation.java.tp.model;

import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.Value;

@Value
public class Equipe {

	private Manager manager;
	private Map<String, List<Developpeur>> developpeurs;
	
}
