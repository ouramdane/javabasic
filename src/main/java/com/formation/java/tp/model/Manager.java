package com.formation.java.tp.model;

import lombok.Value;

@Value
public class Manager extends Employee {
	
	private String nom;
	private String prenom;
	private int age;
	private int expercience;


	public void afficherDetails() {
		System.out.println("Je m'appelle" + getNom()+" "+ getPrenom() + ". J'ai" + getAge() + " ans. je suis un manager");
	}
	
	public static boolean isManager() {
		return true;
	}

	public void getManagerDescription() {
		System.out.println("Je suis un manager: " + isManager());
	}
}
