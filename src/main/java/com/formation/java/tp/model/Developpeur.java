package com.formation.java.tp.model;

import java.util.List;

import com.formation.java.tp.service.Prestataire;

import lombok.Value;

@Value
public class Developpeur extends Employee implements Prestataire {
	
	private String nom;
	private String prenom;
	private int age;
	public boolean isPrestataire;
	private DevCategorie devCategorie;
	private List<String> langages;
	
	@Override
	public void afficherDetails() {
		System.out.println("Je suis " + getNom().toUpperCase() + " " + getPrenom() + ". J'ai " + getAge()
				+ " ans. Je suis un developpeur.");
	}

	public boolean isPrestataire() {

		return isPrestataire;
	}
}
