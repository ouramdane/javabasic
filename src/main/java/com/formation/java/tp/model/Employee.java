/**
 * 
 */
package com.formation.java.tp.model;

/**
 * @author lydiasaighi
 *
 */

public abstract class Employee {

	public abstract void afficherDetails();

	public static boolean isManager() {
		return false;
	}

	public void getEmployeeDescription() {
		System.out.println("Je susi un manager: " + isManager());
	}
}
