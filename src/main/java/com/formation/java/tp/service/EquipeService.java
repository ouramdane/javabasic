package com.formation.java.tp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.formation.java.tp.model.DevCategorie.BACK;
import static com.formation.java.tp.model.DevCategorie.FRONT;

import com.formation.java.tp.exception.EquipecompositionException;
import com.formation.java.tp.model.Developpeur;
import com.formation.java.tp.model.Employee;
import com.formation.java.tp.model.Equipe;
import com.formation.java.tp.model.Manager;

public class EquipeService {

	public Equipe creerEquipe(List<Employee> employees) throws EquipecompositionException {

		Manager manager = null;
		List<Developpeur> backTeam = new ArrayList<>();
		List<Developpeur> frontTeam = new ArrayList<>();

		for (Employee employee : employees) {

			if (employee instanceof Developpeur) {

				creerDevTeams((Developpeur) employee, backTeam, frontTeam);

			}

			if (employee instanceof Manager && manager == null
					&& hasManagerMoreThanTenYearsOfExperience((Manager) employee)) {

				manager = (Manager) employee;

			}
		}
		
		if (manager == null || backTeam.size() != 2 || frontTeam.size() != 2) {
			throw new EquipecompositionException("Impossible de créer une équipe!");
		}

		return construireEquipe(manager, backTeam, backTeam);
	}

	private Equipe construireEquipe(Manager manager, List<Developpeur> backTeam, List<Developpeur> frontTeam) {

		Map<String, List<Developpeur>> devTeam = new HashMap<>();

		devTeam.put(BACK.toString(), backTeam);
		devTeam.put(FRONT.toString(), frontTeam);

		return new Equipe(manager, devTeam);
	}

	private void creerDevTeams(Developpeur developpeur, List<Developpeur> teamBack, List<Developpeur> teamFront) {

		boolean isDevMasteringAtLeastThreeLanguages = isDevMasteringAtLeastThreeLanguages(developpeur);

		if (teamBack.size() <= 2 && isDevMasteringAtLeastThreeLanguages && BACK.equals(developpeur.getDevCategorie())) {
			teamBack.add(developpeur);
		}

		if (teamFront.size() <= 2 && isDevMasteringAtLeastThreeLanguages
				&& FRONT.equals(developpeur.getDevCategorie())) {
			teamFront.add(developpeur);
		}
	}

	private boolean isDevMasteringAtLeastThreeLanguages(Developpeur dev) {

		return dev.getLangages().size() >= 3;
	}

	private boolean hasManagerMoreThanTenYearsOfExperience(Manager manager) {

		return manager.getExpercience() >= 10;
	}
}
