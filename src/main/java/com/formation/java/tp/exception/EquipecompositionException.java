package com.formation.java.tp.exception;

public class EquipecompositionException extends Exception {
	
	
	private static final long serialVersionUID = 1L;
	
	public EquipecompositionException() {
		super();
		 }
		public EquipecompositionException(Exception e) { 
		super(e);
		}
		public EquipecompositionException(String message) {
		super(message); }

}
