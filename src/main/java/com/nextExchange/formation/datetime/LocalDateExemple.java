package com.nextExchange.formation.datetime;

import java.time.LocalDate;
import java.time.Month;

public class LocalDateExemple {

	public static void main(String[] args) {
		
		LocalDate date1 = LocalDate.of(2019, Month.AUGUST, 20); 
		LocalDate date2 = LocalDate.of(2019, 8, 20); 
		
		System.out.println(date1);
		System.out.println(date2);

	}

}
