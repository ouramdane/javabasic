package com.nextExchange.formation.datetime;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

public class PeriodExemple {

	public static void main(String[] args) {
		LocalDate start = LocalDate.of(2015, Month.JANUARY, 1);
		
		
		

	
		
		Period annually = Period.ofYears(1); 
		Period everyThreeWeeks = Period.ofWeeks(3);
		Period everyMonth = Period.ofMonths(1); 
		Period everyOtherDay = Period.ofDays(2);
		Period everyYearAndAWeek = Period.of(1, 0, 7);
		
		start = start.plus(everyMonth);
		
		System.out.println(start);
	}

}
