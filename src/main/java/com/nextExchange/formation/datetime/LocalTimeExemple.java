package com.nextExchange.formation.datetime;

import java.time.LocalTime;

public class LocalTimeExemple {

	public static void main(String[] args) {
		
		LocalTime time1 = LocalTime.of(14, 30); // hour and minute 
		LocalTime time2 = LocalTime.of(14, 30, 30); 	// + seconds
		LocalTime time3 = LocalTime.of(14, 30, 30, 200); 	// + nanoseconds 
	
		System.out.println(time1);
		System.out.println(time2);
		System.out.println(time3);

	}

}
