package com.nextExchange.formation.datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class LocalDateTimeExemple {

	public static void main(String[] args) {
		
		LocalTime time1 = LocalTime.of(14, 30); 
		LocalDate date1 = LocalDate.of(2019, Month.AUGUST, 20); 
		
		LocalDateTime dateTime1 = LocalDateTime.of(2019, Month.AUGUST, 20, 14, 15, 30); 
		LocalDateTime dateTime2 = LocalDateTime.of(date1, time1); 
		
		
		System.out.println(dateTime1);
		System.out.println(dateTime2);

	}

}
