package com.nextExchange.formation.datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class FormattingExemple {

	public static void main(String[] args) {
		
		LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
		System.out.println(date);

		LocalTime time = LocalTime.of(11, 12, 34);
		System.out.println(time.format(DateTimeFormatter.ISO_TIME));
		
		LocalDateTime dateTime = LocalDateTime.of(date, time);


		DateTimeFormatter shortF = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
		System.out.println(shortF.format(dateTime));
		
		DateTimeFormatter mediumF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);	
		System.out.println(mediumF.format(dateTime));
		

		DateTimeFormatter f = DateTimeFormatter.ofPattern("MMMM dd, yyyy, hh:mm");
		System.out.println(dateTime.format(f)); 

	}

}
