package com.nextExchange.formation.datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class ManipDateTimeExemple {

	public static void main(String[] args) {
		LocalDate date = LocalDate.of(2019, Month.AUGUST, 20);
		System.out.println(date);
		date = date.plusDays(2);
		System.out.println(date);
		date = date.plusWeeks(1);
		System.out.println(date);
		date = date.plusMonths(1);
		System.out.println(date);
		date = date.plusYears(5);
		System.out.println(date);

		LocalTime time = LocalTime.of(5, 15);
		LocalDateTime dateTime = LocalDateTime.of(date, time);
		System.out.println(dateTime);
		dateTime = dateTime.minusDays(1);
		System.out.println(dateTime);
		dateTime = dateTime.minusHours(10);
		System.out.println(dateTime);
		dateTime = dateTime.minusSeconds(30);
		System.out.println(dateTime);
		
		
		LocalDateTime dateTime2 = LocalDateTime.of(date, time)
				.minusDays(1).minusHours(10).minusSeconds(30);
		
		System.out.println(dateTime2);
		
		//date = date.plusMinutes(1); // DOES NOT COMPILE
		
	}

}
