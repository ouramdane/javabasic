/**
 * 
 */
package com.nextExchange.formation.strings;

/**
 * @author lydiasaighi
 *
 */
public class StringExamples {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String str1 = "Hello";                    // String pool
		String str2 = new String ("Hello");       //Heap
		
		//Equality
		System.out.println(str1 == str2);  
		System.out.println(str1.equals(str2));
		
		//Other Strings Operations
		
		//Immutability
		String str3 = str2.concat("EveryOne");
		
		System.out.println("str2 = "+ str2);
		System.out.println("str3 = "+ str3);

	}

}
