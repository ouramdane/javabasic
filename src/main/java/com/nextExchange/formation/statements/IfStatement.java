/**
 * 
 */
package com.nextExchange.formation.statements;

/**
 * @author lydiasaighi
 *
 */
public class IfStatement {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Exemple 1 : The if-then Statement

		int hourOfDay = 10;

		if (hourOfDay < 12) {
			System.out.println("Good Morning");
		}

		// Exemple 2 : if-then-else Statement

		if (hourOfDay < 12) {
			System.out.println("Good Morning");
		} else {
			System.out.println("Good Afternoon");
		}

		// Exemple 3 : Ternary Operator

		String greeting = (hourOfDay < 12) ? "Good Morning" : "Good Afternoon";

		//int animal = (y < 91) ? 9 : "Horse";

	}

}
