/**
 * 
 */
package com.nextExchange.formation.statements;

/**
 * @author lydiasaighi
 *
 */
public class SwitchStatement2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int dayOfWeek = 5; 
		switch(dayOfWeek) { 
		case 0: 
		System.out.println("Sunday"); 
		default: 
		System.out.println("Weekday"); 
		case 6: 
		System.out.println("Saturday");
		 break; 
		} 


	}

}
