/**
 * 
 */
package com.nextExchange.formation.statements;

/**
 * @author lydiasaighi
 *
 */
public class ForStatement1 {

	public static void main(String[] args) {

		// Exemple 1 : Basic For Statement

		for (int i = 0; i < 10; i++) {
			System.out.print(i + " ");
		}
		
		// Exemple 1 : Adding Multiple Terms to the for Statement

		int x = 0;
		for (long y = 0, z = 4; x < 5 && y < 10; x++, y++) {
			System.out.print(y + " ");
		}
		System.out.print(x);

		// Exemple 3 : Redeclaring a Variable in the Initialization Block
		
		int x1 = 0;
		for (long y1 = 0, x1 = 4; x1 < 5 && y1 < 10; x1++, y1++) {// DOES NOT COMPILE
			System.out.print(x1 + " ");
		}

		int x2 = 0;
		long y2 = 10;
		for (y2 = 0, x2 = 4; x2 < 5 && y2 < 10; x2++, y2++) {
			System.out.print(x2 + " ");
		}

		// Exemple 4 : Using Incompatible Data Types in the Initialization Block
		for (long y3 = 0, x3 = 4; x3 < 5 && y3 < 10; x3++, y3++) {

			System.out.print(x3 + " ");
		}

		// Exemple 5 : Creating an Infinite Loop
		for (;;) {
			System.out.println("Hello World");
		}

	}

}
