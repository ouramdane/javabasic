/**
 * 
 */
package com.nextExchange.formation.statements;

/**
 * @author lydiasaighi
 *
 */
public class ForStatement2 {

	public static void main(String[] args) {

		// Exemple 1 : The for-each Statement

		final String[] names = new String[3];

		names[0] = "David";
		names[1] = "Sarah";
		names[2] = "Tomas";

		for (String name : names) {
			System.out.print(name + ", ");
		}

		// Example 2 : Nested Loop
		for (int i = 5; i > 0; i--) {

			for (int j = 0; j < 5; j++) {
				System.out.print(i + "  " + j);
			}
		}

	}

}
