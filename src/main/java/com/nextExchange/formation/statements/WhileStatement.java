/**
 * 
 */
package com.nextExchange.formation.statements;

/**
 * @author lydiasaighi
 *
 */
public class WhileStatement {


	public static void main(String[] args) {
		
		//Exemple 1 : The while Statement 
		int i= 5; 

		while (i> 0) { 
		i--; 
		System.out.println("Good morning"); 

		} 
		
		//Exemple 2 : Do While statement
		
		   int j = 0; do { 
			j++;
			} while(false); 

			System.out.println(j); 

			
		//Exemple 3 :
		int x = 2; 
		int y = 5; 

		while(x < 10) 
		y++; 

		} 


}
