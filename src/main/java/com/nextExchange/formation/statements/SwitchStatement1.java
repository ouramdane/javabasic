package com.nextExchange.formation.statements;

public class SwitchStatement1 {

	public static void main(String[] args) {

		// Exemple 1 :
		int dayOfWeek = 5;

		switch(dayOfWeek) { 
		default: System.out.println("Weekday");
		 break; 
		case 0: 
		System.out.println("Sunday"); 
		break; 
		case 6: System.out.println("Saturday");
		 break; 
		} 

	}

}
