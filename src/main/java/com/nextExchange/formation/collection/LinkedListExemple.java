package com.nextExchange.formation.collection;
import java.util.LinkedList;
import java.util.List;

public class LinkedListExemple {

	public static void main(String[] args) {

		List<String> months = new LinkedList<>();
		months.add("Janvier");
		months.add("Aout");
		months.add("Septembre");

		for (String month : months) {
			System.out.println(month);
	}
	}
}