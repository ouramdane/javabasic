package com.nextExchange.formation.collection;

import java.util.HashSet;
import java.util.Set;

public class HashSetExemple {
	public static void main(String[] args) {
		
		// Créer une HashSet
        Set<String> months = new HashSet<>();

        // Ajouter des élements 
        months.add("Janvier");
        months.add("Juin");
        months.add("Aout");
        months.add("Septembre");

        // Ajouter un doublon -> Il sera ignoré
        months.add("Septembre");

        System.out.println(months);
	}
}
