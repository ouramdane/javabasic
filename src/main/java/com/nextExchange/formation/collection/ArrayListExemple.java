package com.nextExchange.formation.collection;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExemple {

	public static void main(String[] args) {

		List<String> months = new ArrayList<>();
		months.add("Janvier");
		months.add("Aout");
		months.add("Septembre");

		for (String month : months)
			System.out.println(month);
	}
}