package com.nextExchange.formation.collection;

import java.util.HashMap;
import java.util.Map;

public class HashMapExemple {

  public static void main(String[] args) {

    Map<Integer, String> map = new HashMap<>();
    map.put(1, "Janvier");
    map.put(10, "Octobre");
    map.put(12, "Decembre");
    map.put(8, "Aout");
    

    for (String i : map.values()) {
    	  System.out.println(i);
    	}
}
}