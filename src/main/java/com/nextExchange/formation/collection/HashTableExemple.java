package com.nextExchange.formation.collection;

import java.util.Hashtable;
import java.util.Map;

public class HashTableExemple {

	public static void main(String[] args) {

		Map<String, String> ht = new Hashtable<>();
		ht.put("01", "Janvier");
		ht.put("10", "Octobre");
		ht.put("12", "Decembre");
		ht.put("08", "Aout");

		System.out.println("   Cle   |   Hash   |  Valeur");
		for (String k : ht.keySet()) {
			System.out.println("   "+k +"        "+k.hashCode() +"     " + ht.get(k));
		}
	}
}