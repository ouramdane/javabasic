#Immutability

Once a String object is created, it is not allowed to change. It cannot be made larger or smaller, and you cannot change one of the characters inside it. 

Mutable is another word for changeable. 

#String
The String class is such a fundamental class that you’d be hard-pressed to write code with- out it. After all, you can’t even write a main() method without using the String class. A string is basically a sequence of characters; here’s an example: 



